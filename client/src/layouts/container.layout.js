import React from 'react';
import { Grid, Container } from '@material-ui/core';


export default function ContainerLayout (props) {
    const { children, className } = props;
    return (
        <Grid>
            <Container className={className} maxWidth="md">
                {children}
            </Container>
        </Grid>
    );
}