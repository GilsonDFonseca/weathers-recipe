import { combineReducers } from 'redux';
import recipeReducer from './recipe';


export default combineReducers({
    recipe: recipeReducer,
});