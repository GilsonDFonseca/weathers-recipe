import { recipeService } from '../../service/recipe.service';

// CONST
export const Types = {
    GETRECIPES: 'recipeService/GETRECIPES',
    LOADING: 'recipeService/LOADING',
    ERROR: 'recipeService/ERROR',
};

// INITIAL STATE

const initialState = {
    recipes: [],
    loading: false,
    error: {
        error: false,
        message: '',
        status: null,
    },
}



// REDUCER
export default function reducer (state = initialState, action) {
    switch (action.type) {
        case Types.GETRECIPES:
            return {
                ...state,
                recipes: action.payload.recipes,
                loading: action.payload.loading
            }
        case Types.LOADING:
            return {
                ...state,
                loading: action.payload.loading,
                error: action.payload.error,
                message: action.payload.message,
                status: action.payload.status
            }
        case Types.ERROR:
            return {
                ...state,
                loading: action.payload.loading,
                error: action.payload.error,
                message: action.payload.message,
                status: action.payload.status
            }
        default:
            return state;
    }
}


// ACTIONS
function getRecipes (params) {
    return dispatch => {
        dispatch({
            type: Types.LOADING,
            payload: {
                error: {
                    error: false,
                    message: '',
                    status: null,
                },
                loading: true
            }
        });

        return recipeService.getRecipes(params).then(data => {
            dispatch({
                type: Types.GETRECIPES,
                payload: {
                    recipes: data.hits,
                    loading: false
                }
            });
        }).catch(err => {
            console.log(err)
            dispatch({
                type: Types.ERROR,
                payload: {
                    error: {
                        error: true,
                        message: err.response.data,
                        status: err.response.status,
                    },
                    loading: false
                }
            });
        })
    }
}

export const actions = {
    getRecipes
}
