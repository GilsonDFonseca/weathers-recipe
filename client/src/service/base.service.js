import axios from 'axios';

export const qrService = async (data) => {
  const basepath = process.env.REACT_APP_API_BASEPATH;
  const api = axios.create({
    baseURL: basepath,
  });

  const result = api.post('/', data)
    .then(response => {
      return response.data;
    })
    .catch(err => {
      throw err;
    })
  return result;
}

