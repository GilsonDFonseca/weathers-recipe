import { qrService } from './base.service';

function getRecipes (params) {
    return qrService(params)
}

export const recipeService = {
    getRecipes,
}