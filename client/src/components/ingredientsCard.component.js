import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Grid, CardActionArea, CardMedia, CardContent, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    height: {
        height: '100%',
    },
    width: {
        width: '100%'
    },
    cardContent: {
        height: '5rem',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    cardMedia: {
        height: 140,
        objectFit: 'fill',
    }
}));


export default function IngredientCardComponent (props) {
    const { ingredient } = props;
    const classes = useStyles();
    
    return (
        <Grid item xs={12} sm={3}>
            <Card>
                <CardActionArea disabled>
                    <CardMedia
                        className={classes.cardMedia}
                        component="img"
                        alt={ingredient.text.length !== 0 ? ingredient.text : ''}
                        image={ingredient.image ? ingredient.image : 'https://www.termoparts.com.br/wp-content/uploads/2017/10/no-image.jpg'}
                        title={ingredient.text.length !== 0 ? ingredient.text : ''}
                    />
                    <CardContent className={classes.cardContent} >
                        <Typography
                            align="center"
                            gutterBottom
                            variant="body1"
                            component="h4">
                            {ingredient.text.length !== 0 ? ingredient.text : ''}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    );
}
