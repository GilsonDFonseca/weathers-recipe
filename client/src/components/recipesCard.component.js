import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useReactRouter from 'use-react-router';
import { Card, Grid, CardActionArea, CardMedia, CardContent, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    height: {
        height: '100%',
    },
    width: {
        width: '100%'
    },
    cardContent: {
        height: '5rem',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    cardMedia: {
        height: 140
    }
}));

export default function RecipeCardComponent (props) {
    const { history } = useReactRouter();
    const { recipe } = props;
    const classes = useStyles();
    
    return (
        <Grid item xs={12} sm={3}>
            <Card>
                <CardActionArea 
                    onClick={() => history.push({pathname: '/recipe', state: {recipe}})}
                >
                    <CardMedia
                        className={classes.cardMedia}
                        component="img"
                        alt={recipe.label.length !== 0 ? recipe.label : 'Receita sem nome'}
                        image={recipe.image ? recipe.image : 'https://www.termoparts.com.br/wp-content/uploads/2017/10/no-image.jpg'}
                        title={recipe.label.length !== 0 ? recipe.label : 'Receita sem nome'}
                    />
                    <CardContent className={classes.cardContent} >
                        <Typography
                            noWrap
                            gutterBottom
                            variant="h6"
                            component="h4">
                            {recipe.label.length !== 0 ? recipe.label : 'Receita sem nome'}
                        </Typography>
                        <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p">
                            {'To know more, click here!'}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    );
}
