import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// import { recipeService } from '../service/recipe.service';
import { actions as recipeActions } from '../redux/ducks/recipe';
import ContainerLayout from '../layouts/container.layout';
import CardComponent from '../components/recipesCard.component';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Card, Button, Grid, Typography, CircularProgress } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '0 20',
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
  },

  height: {
    height: '100%',
  },

  width: {
    width: '100%'
  },

  child: {
    borderRadius: 10,
    padding: 20,
  },

  header: {
    display: 'flex',
    flexDirection: 'column',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  content: {
    marginTop: 20,
    display: 'flex',
    flexDirection: 'row',
    height: '100%'
  },
  button: {
    marginTop: 20,
    alignSelf: 'flex-end',
  },

}));


const renderEmptyFeedback = (error) => {
  return (
    <Grid
      container
      spacing={2}
      direction="row"
      justify="center"
      alignItems="center"
      style={{
        overflowY: 'auto',
        height: '80%'
      }}
    >
      <Typography
        variant="h6"
        gutterBottom
        component="h4">
        {error.message}
      </Typography>
    </Grid>
  )
}

const renderInfo = () => {
  return (
    <Grid
      container
      spacing={2}
      direction="row"
      justify="center"
      alignItems="center"
      style={{
        overflowY: 'auto',
        height: '80%'
      }}
    >
      <Typography
        variant="h6"
        gutterBottom
        component="h4">
        {'Enter a city name in the box above and you\'ll receive recipes based on its temperature'}
      </Typography>
    </Grid>
  )
}

const renderRecipeCards = (recipes) => {
  return (
    <Grid
      container
      spacing={2}
      direction="row"
      justify="center"
      alignItems="center"
      style={{
        overflowY: 'auto',
        height: '80%'
      }}
    >
      {recipes.map(({ recipe }, index) => {
        return (
          <CardComponent key={recipe.label + '-' + index} recipe={recipe} />
        )
      })}
    </Grid>
  )
}

export default function App () {
  const dispatch = useDispatch();

  const classes = useStyles();
  const { recipes, loading, error } = useSelector(state => state.recipe);
  const [city, setCity] = useState('');
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

  function handleChange (event) {
    setCity(event.target.value);
  }

  function sendRequest () {
    dispatch(recipeActions.getRecipes({ city: city }));
  }

  return (
    <ContainerLayout className={classes.root}>
      <Card elevation={isMobile ? 0 : 4} className={`${classes.child} ${classes.height}`}>
        <Grid item xs={12} className={classes.header}>
          <Typography
            variant="h6"
            gutterBottom
            component="h4">
            {'Weather Recipe'}
          </Typography>

          <TextField
            className={classes.input}
            value={city}
            onChange={handleChange}
            fullWidth
            label="Type a city name here"
            variant="outlined" />

          {!loading
            ? <Button
              disabled={city.length === 0}
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={sendRequest}>
              {'Send'}
            </Button>
            : <CircularProgress className={classes.button} />
          }
        </Grid>
        <Grid item xs={12} className={classes.content}>
          {!loading
            ? error && !error.error
              ? recipes && recipes.length !== 0
                ? renderRecipeCards(recipes, classes)
                : renderInfo()
              : renderEmptyFeedback(error)
            : null
          }
        </Grid>
      </Card>

    </ContainerLayout>

  );
}
