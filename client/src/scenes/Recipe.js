import React from 'react';
import useReactRouter from 'use-react-router';
import { makeStyles } from '@material-ui/core/styles';
import { Card, Grid, AppBar, Toolbar, Typography, IconButton, CardMedia, CardContent } from '@material-ui/core';
import ContainerLayout from '../layouts/container.layout';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IngredientsCard from '../components/ingredientsCard.component';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
        position: 'relative'
    },

    appbar: {
        borderTopLeftRadius: '10px',
        borderTopRightRadius: '10px',
        display: 'flex',
        flexDirection: 'row',
        position: 'absolute',
        background: 'transparent',
        boxShadow: 'none',
        width: '90%',

    },

    menuButton: {
        margin: 10,
        color: 'black',
        boxShadow: '2px 2px #000000',
    },

    height: {
        height: '100%',
    },

    header: {
        display: 'flex',
        flexDirection: 'column',
    },

    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    content: {
        marginTop: 20,
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
    },
    button: {
        marginTop: 20,
        alignSelf: 'flex-end',
    },

    cardMedia: {
        height: 64
    },

    title: {
        textShadow: '2px 2px #000000',
    },
}));


export default function Recipe (props) {
    const { history } = useReactRouter();
    const { recipe } = props.location.state;
    const classes = useStyles();
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <ContainerLayout className={classes.root}>
            <AppBar position="static" className={classes.appbar}>
                <IconButton className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => history.goBack()}>
                    <ArrowBackIcon className={classes.boxShadow} />
                </IconButton >
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        {recipe.label}
                    </Typography>
                </Toolbar>
            </AppBar >
            <Card elevation={isMobile ? 0 : 4} className={classes.height}>
                <CardMedia
                    className={classes.cardMedia}
                    component="img"
                    alt={recipe.label.length !== 0 ? recipe.label : 'Receita sem nome'}
                    image={recipe.image.length !== 0 ? recipe.image : 'https://www.termoparts.com.br/wp-content/uploads/2017/10/no-image.jpg'}
                    title={recipe.label.length !== 0 ? recipe.label : 'Receita sem nome'}
                />
                <CardContent className={classes.content} >
                    <Typography variant="h5" gutterBottom component="h4">
                        {'Ingredients'}
                    </Typography>
                    <Grid
                        container
                        spacing={2}
                        direction="row"
                        style={{
                            overflowY: 'auto',
                            height: '80%',
                        }}
                    >
                        {recipe.ingredients.map((ingredient, index) => {
                            return (<IngredientsCard key={ingredient.text + '-' + index} ingredient={ingredient} />)
                        })}
                    </Grid>
                </CardContent>
            </Card>

        </ContainerLayout>
    );
}
