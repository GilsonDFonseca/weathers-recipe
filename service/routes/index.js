const express = require('express');
const router = express.Router();

const getWeather = require('../functions/getWeather');
const getRecipe = require('../functions/getRecipe');

router.post('/', async (req, res, next) => {
    var weatherResult;
    var recipeResult;
    try {
        weatherResult = await getWeather(req.body.city);
        if (weatherResult && weatherResult.cod === 200) {
            try {
                recipeResult = await getRecipe(weatherResult.main.temp);
                res.send(recipeResult);
            }
            catch (error) {
                res.status(401).send(error.message);
            }
        }
    }
    catch (error) {
        res.status(error.cod).send(error.message);
    }
})

module.exports = router;
