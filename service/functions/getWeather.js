var axios = require('axios');

module.exports = function getWeather (cityName) {
    // CHAVE DA API DO OpenWeather
    const API_KEY = '6e5ff5434e61b695f48dd595777b0e1f';

    // Nova instancia axios com a URL base
    const api = axios.create({
        baseURL: 'http://api.openweathermap.org/data/2.5/',
        params: {
            _limit: 10
        }
    });

    const delay = interval => new Promise(resolve => setTimeout(resolve, interval));

    //Request
    const result = api.get(`weather?q=${cityName}&units=metric&appid=${API_KEY}`)
        .then(async response => {
            await delay(1000); //60 por minuto
            return response.data
        })
        .catch(async error => {
            await delay(1000);
            throw error.response.data;
        })
    return result;
};