var axios = require('axios');

module.exports = function getWeather (temperature) {

    // DADOS DA API DO EDAMAM
    const APP_ID = 'e8b87bd7';
    const APP_KEY = '3de2782991b7b775f569191c19f2d827';

    var recipeType;
    var random = Math.floor(Math.random() * 10)
    temperature = parseFloat(temperature);
    if (temperature < 20) {
        recipeType = random % 2 === 0 ? 'soup' : 'broth';
    } else {
        if (temperature >= 20 && temperature < 30) {
            recipeType = random % 2 === 0 ? 'meat' : 'cereal';
        } else {
            recipeType = 'salad'
        }
    }

    // Nova instancia axios com a URL base
    const api = axios.create({
        baseURL: 'https://api.edamam.com/',
    });


    const delay = interval => new Promise(resolve => setTimeout(resolve, interval));

    //Request
    const result = api.get(`search?q=${recipeType}&app_id=${APP_ID}&app_key=${APP_KEY}&from=0&to=6`)
        .then(async response => {
            await delay(12000); //5 por minuto
            return response.data
        })
        .catch(async error => {
            await delay(12000);
            throw error.response.data;
        })
    return result;
};