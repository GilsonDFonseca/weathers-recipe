## ABOUT
This is a project that shows recipes based on the weather. To do so, the user must enter a city's name, then a call to an API will return the temperature, based on that another call will retrieve recipes.
## INSTALL

To install it, run in root

```npm
npm install
```

## RUN

After install, run in root

```npm
npm start
```
## License
[MIT](https://choosealicense.com/licenses/mit/)